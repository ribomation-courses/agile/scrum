Scrum Project Exercise
====

Objective
----
Within a 1h+ time-frame simulate a scrum project.

Content
----
Here you will find skeleton/template files, code snippets and library files.

Coding Effort
----
Your primary work during the project is to concentrate on the Scrum related issues.
The actual coding effort shouldn't take more than a few minutes, because you will
be given all source code and you just have to assemble it all together.

Preparation
----
One person creates a new project in Scrumwise, a team and then invites all other collaborators.
Finally, invite me (Jens) as a stake-holder, that allows me to show the task board
for all participants during the project.

The same person, should also create a new public GIT repo in GitHub and invite
all team members as collaborators.

Way of Working
----
1. Understand the objectives
1. Write a backlog item for each bullet objective in the course material
1. Perform a sprint planning meeting and a (_fake_) technical design, based on
   how you would have coded it without the given code snippets here.
1. Ensure you a decent set of technical tasks, with estimates, for each backlog item
1. Start the sprint and select tasks to do, based on collaboration and division of duties
1. Complete selected tasks and set fake actual time durations
1. When all tasks are done or the time is up, the sprint is complete
1. Perform a demo of the web-app, as the sprint review
1. Perform a retrospective meeting, as the last part of this project

GitHub Integration
----
Setup the integration _GitHub --> Scrumwise integration and commit/push each
code change with a proper commit message setting the used time and new status.
Foe example

    Created initial HTML files [Scrumwise task #42, used: 1.25h, status: Done]
More information can be found at
* [How to format commit messages](https://scrumwise.helpscoutdocs.com/article/93-how-do-i-format-my-commit-messages)

For the integration to work, it's very important that you are using the _same email
address_ in both Scrumwise and GitHub.

GIT Primer
----
Use a GIT Bash command window for all GIT operations.

Start by clone the project GIT repo

    git clone https://github.com/{username}/{reponame}.git
    cd {reponame}
Above, you should replace _{key}_ with the actual names in your project.

After you have performed some edits and want to share it with the team, you enter
the following commands in the root of the project directory

    git add .
    git commit -m '{commit message, with scrumwise directive}'
    git push

When you push, git will ask you for your GitHub username and password.

All other team members can then get the updated code, with this command

    git pull

Performing a `git pull` can potentially lead to merge conflict. In order to
avoid that, edit different files when editing concurrently.
