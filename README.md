Scrum, 1 day
====

Welcome to this course.
The syllabus can be find at
[agile/scrum](https://www.ribomation.se/agile/scrum.html)

Here you will find
* Installation instructions
* Sources to the scrum project

Preparation Before the Course
====
Create Accounts
----
Please, sign-up for the following services before the course:
* [Scrumwise, _30-day trial_](http://www.scrumwise.com/)
  - Click "Try Now" to open the demo, then click "To keep your account, click here"
    and sign-up for a 30-day trial.
* [GitHub, _free open account_](https://github.com/)
  - As long as you only create public git repos, it's free to use.

Install Software
----
* [Google Chrome](https://www.google.com/intl/sv/chrome/)
  - You need a decent/modern web browser to run Scrumwise and the project
* [GIT Client](https://git-scm.com/downloads)
  - You need to have a GIT client installed to clone the course repo
* [JetBrains WebStorm, _30 day trial_](https://www.jetbrains.com/webstorm/download/#section=windows)
  - As a help for assembling the files of the scrum project, we recommend WebStorm.
    Other tools to use might be [MS Visual Code](https://code.visualstudio.com/download)
    or the [Atom Text Editor](https://atom.io/)

Using Scrumwise
====
The easiest way to start using Scrumwise is that one person signs up for an account
and then invites all other persons that should collaborate together.

Scrumwise do not support using the same email address in different accounts.
However, they provide a nice work-around where you can add an arbitrary _+suffix_
to your email username. You can read more about it at the link below
* [Use the same email address in multiple accounts?](https://scrumwise.helpscoutdocs.com/article/115-can-i-use-the-same-email-address-in-multiple-accounts)


Course GIT Repo
====
Get the course repo initially by a `git clone` operation

    git clone https://gitlab.com/ribomation-courses/agile/scrum.git
    cd scrum

Get the any updates by a `git pull` operation

    git pull



***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
